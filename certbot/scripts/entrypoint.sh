#!/bin/sh

# one-time execution at container start once host's health check is ok
/scripts/run_certbot.sh

# scheduling periodic executions
exec crond -f
