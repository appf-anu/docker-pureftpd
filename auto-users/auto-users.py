#!/usr/bin/env python3
# docker-pureftpd
# Copyright (C) 2016  gimoh, gareth dunstone
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Auto FTP user accounts.

Automatically synchronizes (creates/removes) PureFTP virtual user
accounts with definition in YAML config file

The config should have following schema:

  users:
    username: "password"
    username2: "password2"

Usage:
  auto-users [options] CONFIG_DIR

Options:
  -h --help       This help text
  -d --dry-run    only show what accounts would be created
  -D --no-delete  do not delete accounts even if they don't exist in config
"""


from pipes import quote
from subprocess import CalledProcessError
import yaml


from docopt import docopt
from shell import instream, p

import logging
import structlog
import sys
import os
import time
import glob
import collections.abc


def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


logging.basicConfig(
    format="%(message)s",
    level=os.environ.get("LOG_LEVEL", "INFO"),
    stream=sys.stdout
)

structlog.configure(
    processors=[
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_log_level_number,
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.JSONRenderer()
    ],
    context_class=dict,
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)

logger = structlog.get_logger("pureftpd-auto-users")


def _check_call(proc):
    """
    :type proc: shell.RunCmd
    :raises: CalledProcessError if returncode != 0
    """
    if proc.re() != 0:
        raise CalledProcessError(
            proc.re(), proc.cmd_str,
            proc.stdout().decode('utf-8'), proc.stderr().decode('utf-8'))
    return proc


class PurePW:
    """Represents a PureFTP password database"""

    def commit(self):
        """Commit any changes to the binary DB file"""
        _check_call(p('pure-pw mkdb'))

    def list(self):
        """A list of usernames currently in the password database"""
        try:
            proc = _check_call(p('pure-pw list'))
            out = proc.stdout().decode('utf-8')
        except CalledProcessError as e:
            if e.returncode != 2 or \
                    'Unable to open the passwd file:' not in e.stderr:
                raise
            # DB doesn't exist, so no users
            out = ''

        return [l.split('\t')[0] for l in out.splitlines()]

    def adduser(self, username, password):
        """Add a virtual FTP user to PureFTP database"""
        # need to pass a string like 'password\npassword\n'
        pw_str = '\n'.join([password] * 2 + [''])
        proc = instream(pw_str).p('adduser-ftp {}'.format(quote(username)))
        _check_call(proc)

    def deluser(self, username):
        """Remove a virtual FTP user from PureFTP database"""
        _check_call(p('pure-pw userdel {}'.format(quote(username))))


def sync_users(users, dry_run=False, no_delete=False):
    """Sync (add/remove) FTP users in the DB to the specified list

    :param users: a list of user info dicts containing at least keys:
        username, password
    :param dry_run: if True only print what would have been done
    :param no_delete: if True do not delete accounts which are in DB
        but not in the C{users} list
    """
    pw = PurePW()
    existing = pw.list()

    to_add = {u: p for u, p in users.items() if u not in existing}
    to_rm = [u for u in existing if u not in users.keys()]

    add = pw.adduser if not dry_run else lambda u, p: logger.warning(f"add dry run {u}:{p}")
    rm = pw.deluser if not dry_run else lambda u: logger.warning(f"remove dry run {u}")
    commit = pw.commit if not dry_run else lambda: logger.warning(f"commit dry run")

    def try_do(func, ok_msg, fail_msg, *args):
        try:
            func(*args)
        except Exception as e:
            logger.error(fail_msg.format(*args), exc_info=e)
        else:
            logger.info(ok_msg.format(*args))

    for user, passwd in to_add.items():
        try_do(add, 'added {}', 'failed to add {}', user, passwd)

    if no_delete:
        commit()
        if to_rm:
            logger.warning('skipping deletion of following users: {}'.format(
                ', '.join(to_rm)))
        return

    for user in to_rm:
        try_do(rm, 'removed {}', 'failed to remove {}', user)
    commit()


def main():
    args = docopt(__doc__, version='0.1')

    confdir = args.get("CONFIG_DIR")
    assert os.path.isdir(confdir), "CONFIG_DIR doesn't exist"
    files = glob.glob(os.path.join(confdir, "*.yaml"))
    files.extend(glob.glob(os.path.join(confdir, "*.yml")))
    cfg = {}
    for fn in files:
        with open(fn) as fh:
            update(cfg, yaml.load(fh, Loader=yaml.SafeLoader))

    users = cfg.get("users", {})
    if cfg.get('ftp'):
        for camera, data in cfg['ftp'].get("cameras", {}).items():
            if data.get("camera_name") and data.get("password"):
                users[data['camera_name']] = data['password']
        for datalogger, data in cfg['ftp'].get("dataloggers", {}).items():
            if data.get("datalogger_name") and data.get("password"):
                users[data['datalogger_name']] = data['password']

    sync_users(
        users,
        dry_run=args['--dry-run'],
        no_delete=args['--no-delete']
    )


if __name__ == '__main__':
    while True:
        main()
        time.sleep(5)
        # rerun every hour
        # time.sleep(60 * 60)
